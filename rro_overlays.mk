# RRO Overlays
PRODUCT_PACKAGES += \
    GoogleSettingsOverlay \
    SetupWizardPixelPrebuiltOverlay

# Pixel Launcher
TARGET_INCLUDE_PIXEL_LAUNCHER ?= true
ifeq ($(TARGET_INCLUDE_PIXEL_LAUNCHER),true)
PRODUCT_PACKAGES += \
    NexusLauncherReleaseOverlay
endif

